# homebridge-noolite-http-rgb

`homebridge-noolite-http-rgb` is a plugin for Homebridge.

Control your `noolite-http`-based RGB light via Homebridge!

## Installation

If you are new to Homebridge, please first read the Homebridge [documentation](https://www.npmjs.com/package/homebridge).
If you are running on a Raspberry, you will find a tutorial in the [homebridge-punt Wiki](https://github.com/cflurin/homebridge-punt/wiki/Running-Homebridge-on-a-Raspberry-Pi).

Install homebridge:
```sh
sudo npm install -g homebridge
```
Install homebridge-noolite-http-rgb:
```sh
sudo npm install -g homebridge-noolite-http-rgb
```

## Configuration

Add the accessory in `config.json` in your home directory inside `.homebridge`.

```js
   {
	"accessory": "NooLite-HTTP-RGB",
  	"name": "RGB LED strip",

	"switch": {
      		"powerOn": "http://<your_ip>/api.htm?ch=<RGBChNumber>&cmd=2",
      		"powerOff": "http://<your_ip>/api.htm?ch=<RGBChNumber>&cmd=0"
      	},

       	"brightness": {
      		"url": "http://<your_ip>/api.htm?ch=<RGBChNumber>&cmd=6&br=%s"
       	},

     	"color": {
       		"url": "http://<your_ip>/api.htm?ch=<RGBChNumber>&cmd=6&fmt=3&d0=%s1&d1=%s2&d2=%s3"
     	}
    }
```

You can omit `http_method`, it defaults to `GET`.

## Note

This plugin based on [homebridge-better-http-rgb](https://www.npmjs.com/package/homebridge-better-http-rgb).

Feel free to contribute to make this a better plugin!
